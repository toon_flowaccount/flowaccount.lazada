"use strict";
const config = require('./config.js')
const request = require('request')

const env = config.environments.open_api
const base_url = env.url
const client_id = env.client_id
const client_secret = env.client_secret
const grant_type = env.grant_type

function saveTaxInvoice(tax_invoice, access_token_fa) {
    return new Promise(async (resolve, reject) => {
        try {
            let options = requestAPI('POST', `${base_url}/tax-invoices`, tax_invoice, access_token_fa)
            resolve(await Request(options));
        } catch (error) {
            reject(error)
        }
    });
}

function requestAPI(method, endpoint, body, access_token_fa) {
    var options = {
        method: method,
        url: endpoint,
        headers: {
            'Authorization': `Bearer ${access_token_fa.access_token}`,
            'Content-type': 'application/json'
        },
        json: true
    };

    if (typeof body !== 'undefined') {
        options["body"] = body;
    }
    return options;
}

async function Request(_options) {
    return new Promise((resolve, reject) => {
        request(_options, function (error, response, body) {
            if (error) reject(error)
            resolve(response);
        });
    });
}

function getTokenByGuid(guid) {
    return new Promise((resolve, reject) => {

        let options = {
            method: 'POST',
            url: `${base_url}/token`,
            form: {
                client_id: client_id,
                client_secret: client_secret,
                grant_type: grant_type,
                guid: guid
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        };

        request(options, function (err, res, body) {
            if (err) reject(err)
            resolve(JSON.parse(body));
        });
    });
}

module.exports = { saveTaxInvoice, getTokenByGuid }