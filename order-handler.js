"use strict";
const config = require('./config.js')
const moment = require('moment')
const env = config.environments.lazop
const LazadaAPI = require('lazada-open-platform-sdk')

function getOrders(access_token) {
    const lazadaAPIWithToken = new LazadaAPI(env.app_key, env.app_secret, 'THAILAND', access_token.access_token)
    return new Promise((resolve, reject) => {
        let prm = {
            created_after: moment(new Date("2019-04-04 14:10:00")).format(),
            // status: 'ready_to_ship',
            status: 'pending',
            sort_direction: 'ASC',
            offset: 0,
            limit: 1
        }
        lazadaAPIWithToken.getOrders(prm)
            .then(res => {
                resolve(res.data.orders)
            })
            .catch(err => {
                reject(err)
            })
    });
}


function getOrderItems(order_id, access_token) {
    const lazadaAPIWithToken = new LazadaAPI(env.app_key, env.app_secret, 'THAILAND', access_token.access_token)
    return new Promise((resolve, reject) => {
        let prm = {
            order_id: order_id.toString()
        }
        lazadaAPIWithToken.getOrderItems(prm)
            .then(res => {
                resolve(res.data)
            })
            .catch(err => reject(err))
    });
}


/**
 * This method for convert order and items of Lazada to json document for preparing data to create document with Flowaccount Open API
 *
 * @param {*} order
 * @param {*} item_list
 * @returns
 */
function mapOrder(order, item_list) {
    return new Promise((resolve, reject) => {
        let sum_tax_amount = 0
        let price = parseFloat(order.price)
        let shipping_fee = parseFloat(order.shipping_fee)
        let sub_total = price + shipping_fee
        let voucher = parseFloat(order.voucher)
        let price_after_discount = sub_total - voucher
        let invoice_item_list = []
        
        item_list.forEach(item => {
            console.log(`Mapping item of ${item.name}`)
            let invoice_item = {
                name: item.name,
                description: item.variation,
                quantity: 1,
                unitName: "ชิ้น",
                pricePerUnit: item.item_price,
                discountAmount: item.voucher_amount,
                vatRate: 7,
                total: item.paid_price
            }

            sum_tax_amount += item.tax_amount
            invoice_item_list.push(invoice_item)
        });

        //If order has shipping fee, it will add the new item for shipping,
        if (order.shipping_fee && order.shipping_fee > 0) {
            invoice_item_list.push({
                name: 'Shipping Fee',
                description: '',
                quantity: 1,
                unitName: "",
                pricePerUnit: order.shipping_fee,
                discountAmount: 0,
                vatRate: 7,
                total: order.shipping_fee
            })
        }

        let tax_invoice = {
			contactCode: order.order_id.toString(),
            contactName: order.customer_first_name,
            contactAddress: `${order.address_billing.address1} ${order.address_billing.address2} ${order.address_billing.address3} ${order.address_billing.address4} ${order.address_billing.address4}`,
            contactTaxId: order.tax_code,
            contactBranch: order.branch_number,
            contactPerson: `${order.customer_last_name} ${order.customer_first_name}`,
            contactEmail: "",
            contactNumber: order.address_billing.phone,
            contactZipCode: order.address_billing.post_code,
            publishedOn: moment(new Date(order.created_at)).format('YYYY-MM-DD'),
            creditType: 1,
            creditDays: 0,
            dueDate: moment(new Date(order.created_at)).format('YYYY-MM-DD'),
            projectName: "Lazada api project",
            reference: order.order_number.toString(),
            isVatInclusive: true,
            salesName: "Lazada",
            discountType: 3,
            items: invoice_item_list,
            subTotal: sub_total,
            discountAmount: voucher,
            totalAfterDiscount: price_after_discount,
            exempAmount: 0,
            vatableAmount: price_after_discount - sum_tax_amount,
            vatAmount: sum_tax_amount,
            grandTotal: price_after_discount,
            remarks: order.remarks,
            internalNotes: ""
        }
        resolve(tax_invoice)
    });
}


/**
 * This method for convert order and items of Lazada to json document for preparing data to create document with Flow advance API
 * @param {*} order order in lazada
 * @param {*} item_list items of the order
 * @returns json tax invoice document
 */
// function mapOrderToTaxInvoice(order, item_list) {
//     return new Promise((resolve, reject) => {
//         try {
//             let sum_tax_amount = 0
//             let price = parseFloat(order.price)
//             let shipping_fee = parseFloat(order.shipping_fee)
//             let sub_total = price + shipping_fee
//             let voucher = parseFloat(order.voucher)
//             let price_after_discount = sub_total - voucher
//             let item_list_invoice = []
//             let index = 0
//             item_list.forEach(item => {
//                 let item_invoice = {
//                     no: index,
//                     id: 0,
//                     description: item.variation,
//                     name: item.name,
//                     productDiscountTypes: 3,
//                     quantity: 1,
//                     vatRate: 7,
//                     discountPerItem: item.voucher_amount,
//                     discountPerItemValue: item.voucher_amount,
//                     total: item.paid_price,
//                     sellChartOfAccountId: 0,
//                     buyChartOfAccountId: 0,
//                     pricePerUnit: item.item_price,
//                     unitName: 'ชิ้น',
//                     isVat: true,
//                 }

//                 item_list_invoice.push(item_invoice)
//                 sum_tax_amount += item.tax_amount
//                 index++
//             });

//             //If order has shipping fee, it will add the new item for shipping,
//             if (order.shipping_fee && order.shipping_fee > 0) {
//                 item_list_invoice.push({
//                     no: index++,
//                     id: 0,
//                     description: '',
//                     name: 'Shipping Fee',
//                     productDiscountTypes: 3,
//                     quantity: 1,
//                     vatRate: 7,
//                     discountPerItem: 0,
//                     discountPerItemValue: 0,
//                     total: order.shipping_fee,
//                     sellChartOfAccountId: 0,
//                     buyChartOfAccountId: 0,
//                     pricePerUnit: order.shipping_fee,
//                     isVat: true
//                 })
//             }


//             let tax_invoice = {
//                 isReCalculate: false,
//                 name: 'Lazada',
//                 documentType: 7,
//                 recordId: 0,
//                 contactId: 0,
//                 contactCode: '',
//                 contactName: `${order.customer_first_name} ${order.customer_last_name}`,
//                 contactNumber: order.address_billing.phone,
//                 contactAddress: `${order.address_billing.address1} ${order.address_billing.address3} ${order.address_billing.address4} ${order.address_billing.post_code} ${order.address_billing.country}`,
//                 contactAddressLine2: null,
//                 contactAddressLine3: null,
//                 contactOriginAddress: null,
//                 //contactShippingAddress: `${order.address_shipping.address1} ${order.address_shipping.address3} ${order.address_shipping.address4} ${order.address_shipping.post_code} ${order.address_shipping.country}`,
//                 //contactNumberOffice: order.address_billing.phone2,
//                 //contactNumberFax: null,
//                 contactGroup: 1,
//                 // contactPerson: null,
//                 contactTaxId: order.tax_code,
//                 contactBranch: order.branch_number,
//                 //contactEmail: null,
//                 contactStateChange: false,
//                 companyStateChange: false,
//                 publishedOn: moment(new Date(order.created_at)).format('YYYY-MM-DD'),
//                 dueDate: moment(new Date(order.created_at)).format('YYYY-MM-DD'),
//                 discount: order.voucher,
//                 discountPercentage: 0,
//                 documentContactCompanyChangeType: 7,
//                 creditDays: 0,
//                 creditType: 1,
//                 vatRate: 0,
//                 isDicountAsPercentage: true,
//                 productItems: item_list_invoice,
//                 status: 1,
//                 // tax: 0, 
//                 // withHeld: 0, 
//                 isWithholding: false,
//                 isBatchDocument: false,
//                 documentDiscountTypes: 3,
//                 inlineDiscountValue: voucher,
//                 inlineVatValue: sum_tax_amount,
//                 useInlineDiscount: true,
//                 useInlineVat: true,
//                 isVatInclusive: true,
//                 showSignatureOrStamp: true,
//                 // media: [],
//                 withholdingTaxItems: 0,
//                 partialPaymentMethod: 1,
//                 partialPercent: 0,
//                 partialAmount: 0,
//                 isVat: true,
//                 expenseCategoryViewType: 3,
//                 sub_total: sub_total,
//                 discountAmount: voucher,
//                 totalAfterDiscount: price_after_discount,
//                 vatValue: sum_tax_amount,
//                 exemptAmount: 0,
//                 vatableAmount: price_after_discount - sum_tax_amount,
//                 grandTotal: price_after_discount,
//                 totalExcludingVat: price_after_discount - sum_tax_amount,
//                 withholdingTaxAmount: 0,
//                 paymentAmount: price_after_discount,
//                 // documentSerial: "",
//                 vatAmount: sum_tax_amount,
//                 total: price_after_discount,
//                 entity: 0,
//                 reference: order.order_number,
//                 remarks: `ข้อมูลใน Lazada หมายเลขการสั่งซื้อ: ${order.order_number} วันที่สั่งซื้อ: ${moment(new Date(order.created_at)).format('YYYY-MM-DD HH:mm')} หมายเหตุ: ${order.remarks}`,
//                 // saleId: 0, 
//                 // saleName: 'Lazada', 
//                 textOther: '',
//                 vesion: 2,
//                 withholdingTaxItems: [{
//                     description: null,
//                     incomeType: 0,
//                     isVatInclusive: false,
//                     taxAmount: 0,
//                     taxAmountNoVat: 0,
//                     taxRate: -2,
//                     total: 0
//                 }],
//             }

//             resolve(tax_invoice)
//         } catch (error) {
//             console.log(error);
//             reject(error)
//         }

//     });
// }

module.exports = { getOrders, getOrderItems, mapOrder }