"use strict";

const express = require("express");
const dynamo_db_handler = require("./dynamo-db-handler")
const app = express();
const order_handler = require("./order-handler")
const open_api_handler = require("./open-api-handler")

app.listen(5000, () => {
    console.log("Server running on port 5000");
});

app.get('/', async function (req, res) {
    try {
        let response = []
        let access_token_list = await dynamo_db_handler.getTokens()
        console.log('Get all tokens in Dynamo DB: ', access_token_list)
        for (const access_token of access_token_list.Items) {
            
            // Get token by Guid from Flowaccount
            let access_token_flow_account = await open_api_handler.getTokenByGuid(access_token.guid)
            console.log(`Get the access token by Guid (${access_token.guid}) from open-api: `, access_token_flow_account);
            
            //Get seller's orders from lazada
            let order_list = await order_handler.getOrders(access_token)
            console.log(`Get orders of seller from lazada and number of orders are ${order_list.length}`)

            for (const order of order_list) {
                //Get items of by order id from lazada
                let item_list = await order_handler.getOrderItems(order.order_id, access_token)
                console.log(`Get items of order (${order.order_id}) from lazada and number of items are ${item_list.length}`)

                //Mapping Order to Tax Invoice
                let tax_invoice = await order_handler.mapOrder(order, item_list)
                console.log('Convert order (${order.order_id}) to json tax invoice document:', tax_invoice)

                // Create tax invoice document
                let result = await open_api_handler.saveTaxInvoice(tax_invoice, access_token_flow_account)
                console.log(`Created tax invoice document by lazada order id ${order.order_id}`)

                response.push(tax_invoice)
            }
        }
        res.send(response)
    } catch (error) {
        res.status(500).send(error)
    }

})

