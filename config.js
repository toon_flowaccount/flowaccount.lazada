require('dotenv').config()
const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-southeast-1' });
// These are environments were encrypted on AWS.
const key_list = ['HUBSPOT_API_KEY', 'MSSQL_PASSWORD', 'MSSQL_USER']

var environments = {
    lazop: {
        app_key: process.env.LAZOP_APP_KEY,
        app_secret: process.env.LAZOP_APP_SECRET
    },
    open_api:{
        url: process.env.FLOWACC_URL,
        client_id: process.env.FLOWACC_CLIENT_ID,
        client_secret: process.env.FLOWACC_CLIENT_SECRET,
        grant_type: process.env.FLOWACC_GRANT_TYPE
    }
}

async function decryptKeys(key_list) {

    return new Promise(async (resolve, reject) => {
        try {
            for (const key of key_list) {
                let decrypted = await decryptKeyByKMS(key)
                switch (key) {
                    case 'DYNAMODB_USER':
                        environments.dynamo_db.username = decrypted
                        break;
                    case 'DYNAMODB_PASSWORD':
                        environments.dynamo_db.password = decrypted
                        break;
                    default:
                        break;
                }
            }
            resolve(true)
        } catch (error) {
            reject(error)
        }

    })
}

function decryptKeyByKMS(encrypted) {
    return new Promise((resolve, reject) => {
        try {
            let decrypted
            const kms = new AWS.KMS();
            kms.decrypt({ CiphertextBlob: new Buffer(process.env[encrypted], 'base64') }, (err, data) => {
                if (err) {
                    reject(err);
                }
                decrypted = data.Plaintext.toString('ascii');
                resolve(decrypted);
            });
        } catch (error) {
            reject(error)
        }
    })
}

function initialConfig() {
    return new Promise(async (resolve, reject) => {
        try {
            await decryptKeys(key_list)
            resolve(true)
        } catch (error) {
            reject(error)
        }

    })
}

module.exports = { initialConfig, environments };