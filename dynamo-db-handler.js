"use strict";
const AWS = require('aws-sdk')

AWS.config.update({
  region: 'ap-southeast-1',
  endpoint: 'http://localhost:8001'
  // accessKeyId: 'toonKeyId',
  // secretAccessKey: 'toonKeySecret'
});

const docClient = new AWS.DynamoDB.DocumentClient();
const table_name = 'LazadaTokens'

function getTokens() {
    return new Promise((resolve, reject) => {
        let params = {
            TableName: table_name
          }
          docClient.scan(params, function (err, data) {
            if (err) {
              reject(err)
            } else {
              resolve(data)
            }
          });
    });
}

module.exports = { getTokens }